package info.androidhive.mantao;

import info.androidhive.slidingmenu.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class BtsNoTrafficFragment extends Fragment {
	
	public BtsNoTrafficFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_bts_no_traffic, container, false);
         
        return rootView;
    }
}
