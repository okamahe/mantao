package info.androidhive.mantao.model;

import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Fragment;

public class CellDownList extends Fragment{


	String neName;
	String info;
	String bscName;
	String neId;
	String dateAlarm;
	String timeAlarm;
	


	public String getBscName() {
		return bscName;
	}



	public void setBscName(String bscName) {
		this.bscName = bscName;
	}



	public String getNeId() {
		return neId;
	}



	public void setNeId(String neId) {
		this.neId = neId;
	}



	public String getDateAlarm() {
		return dateAlarm;
	}



	public void setDateAlarm(String dateAlarm) {
		this.dateAlarm = dateAlarm;
	}



	public String getTimeAlarm() {
		return timeAlarm;
	}



	public void setTimeAlarm(String timeAlarm) {
		this.timeAlarm = timeAlarm;
	}



	public String getNeName() {
		return neName;
	}



	public void setNeName(String neName) {
		this.neName = neName;
	}



	public String getInfo() {
		return info;
	}



	public void setInfo(String info) {
		this.info = info;
	}



	public static LinkedList<CellDownList> getCellDownListFragment(String response){
		LinkedList<CellDownList> list = null;
		
		try {
			JSONObject jObject = new JSONObject(response);
			JSONArray jsonArray = jObject.getJSONArray("data");
			if (jsonArray.length() > 0) {
				list = new LinkedList<CellDownList>();
				CellDownList item = null;
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject object = jsonArray.getJSONObject(i);
					
					item = new CellDownList();
					item.setNeName(object.optString("neName"));
					item.setInfo(object.optString("info"));
					item.setBscName(object.optString("bscName"));
					item.setNeId(object.optString("bcf"));
					item.setDateAlarm(object.optString("tanggal"));
					item.setTimeAlarm(object.optString("jam"));
					list.add(item);
					
				}
				
				
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return list;
	}	
	
	
}
