package info.androidhive.mantao.model;

import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Fragment;

public class NsaList extends Fragment{

	String nsaName;
	String countAlarm;

	public String getCountAlarm() {
		return countAlarm;
	}

	public void setCountAlarm(String countAlarm) {
		this.countAlarm = countAlarm;
	}

	public String getNsaName() {
		return nsaName;
	}

	public void setNsaName(String nsaName) {
		this.nsaName = nsaName;
	}
	
	public static LinkedList<NsaList> getNsaFragment(String response){
		LinkedList<NsaList> list = null;
		
		try {
			JSONObject jObject = new JSONObject(response);
			JSONArray jsonArray = jObject.getJSONArray("data");
			if (jsonArray.length() > 0) {
				list = new LinkedList<NsaList>();
				NsaList item = null;
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject object = jsonArray.getJSONObject(i);
					
					item = new NsaList();
					item.setNsaName(object.optString("nsa"));
					item.setCountAlarm(object.optString("count"));
					list.add(item);
					
				}
				
				
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return list;
	}	
	
	
}
