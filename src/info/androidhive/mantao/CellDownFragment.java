package info.androidhive.mantao;


import info.androidhive.mantao.adapter.CellDownAdapter;
import info.androidhive.mantao.adapter.NsaAdapter;
import info.androidhive.mantao.model.CellDownList;
import info.androidhive.mantao.model.NsaList;
import info.androidhive.slidingmenu.R;

import java.util.LinkedList;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class CellDownFragment extends Fragment {
	
	private ListView lvItem;
	private ProgressBar indicator;
	private LinkedList<CellDownList> listItem;
	private RequestQueue queue = null;
	
	public static String URL ="http://itopkal.com/api/json_cell_down_per_bts.php?region=puma&nsa=";
//	public static String URL ;

	
	public CellDownFragment(){
		}
	
//	private Context getApplicationContext() {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
	private void getNsaData(){
		
		
		lvItem.setVisibility(View.GONE);
		indicator.setVisibility(View.VISIBLE);
		
		// ambil dari volley
		StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>(){

			@Override
			public void onResponse(String response) {
				// TODO Auto-generated method stub
				lvItem.setVisibility(View.VISIBLE);
				indicator.setVisibility(View.GONE);
				
				parsingJson(response);
				
			}

		}, new Response.ErrorListener(){

			@Override
			public void onErrorResponse(com.android.volley.VolleyError error) {
				// TODO Auto-generated method stub
				lvItem.setVisibility(View.GONE);
				indicator.setVisibility(View.GONE);
				Toast.makeText(CellDownFragment.this.getActivity(), "Data tidak ada", Toast.LENGTH_LONG).show();
				
			}
			
		});
		
		queue.add(stringRequest);
	}
	
	private static final String TAG = "MyActivity";
	
	protected void parsingJson (String response) {
		listItem = CellDownList.getCellDownListFragment(response);
		if (listItem.size() > 0) {
			
			
			Log.v(TAG, "testtttt");
			CellDownAdapter adapter = new CellDownAdapter(CellDownFragment.this.getActivity(), listItem);

			
			lvItem.setAdapter(adapter);
			
		}		
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		Log.v(TAG, "testtttt");
		
		View rootView = inflater.inflate(R.layout.fragment_nsa, container, false);
		
		lvItem = (ListView)rootView.findViewById(R.id.lv_item);
		indicator = (ProgressBar)rootView.findViewById(R.id.pb_indicator);
                 
        return rootView;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		listItem = new LinkedList<CellDownList>();
		queue = Volley.newRequestQueue(CellDownFragment.this.getActivity());
		
		getNsaData(); // ambil data dari
		
		lvItem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
//				Intent intent = new Intent(MainActivity.this, BranchDetailActivity.class);
//				intent.putExtra(BranchDetailActivity.KEY_BRANCH, listItem.get(position).getBranch());
//				intent.putExtra(BranchDetailActivity.KEY_BRANCH, listItem.get(position).getRevenue());
//				startActivityForResult(intent, 0);
//				startActivities(intent);
				
				Toast.makeText(CellDownFragment.this.getActivity(), listItem.get(position).getNeName(), Toast.LENGTH_LONG).show();
							
			}
			
		});
	}
}
