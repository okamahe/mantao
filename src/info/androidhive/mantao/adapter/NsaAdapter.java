package info.androidhive.mantao.adapter;
import info.androidhive.mantao.model.NsaList;
import info.androidhive.slidingmenu.R;

import java.util.LinkedList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class NsaAdapter extends BaseAdapter{
	Activity activity;
	LinkedList<NsaList> list;
	public static String BASE_IMAGE_URL = "http://itopkal.com/api/json_cell_down.php?region=puma&alarmCode=7767";
	
	public NsaAdapter(Activity activity, LinkedList<NsaList> list){
		this.list = list;
		this.activity = activity;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		ViewHolder holder = null;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.item_nsa, null);
			holder.txtNsa = (TextView)view.findViewById(R.id.txt_item_nsa);
			holder.txtCount = (TextView)view.findViewById(R.id.txt_counter);
			view.setTag(holder);
		}else {
			holder = (ViewHolder)view.getTag();
		}
		
		holder.txtNsa.setText(list.get(position).getNsaName());
		holder.txtCount.setText(list.get(position).getCountAlarm());
		return view;
		
	}
	
	static class ViewHolder{
		TextView txtNsa, txtCount;
	}
	

}
