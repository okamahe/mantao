package info.androidhive.mantao.adapter;
import info.androidhive.mantao.model.CellDownList;
import info.androidhive.slidingmenu.R;

import java.util.LinkedList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CellDownAdapter extends BaseAdapter{
	Activity activity;
	LinkedList<CellDownList> list;
	public static String BASE_IMAGE_URL = "http://itopkal.com/api/json_cell_down_per_bts.php?region=puma&nsa=Jayapura";
	
	public CellDownAdapter(Activity activity, LinkedList<CellDownList> list){
		this.list = list;
		this.activity = activity;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		ViewHolder holder = null;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.item_cell_down, null);
			holder.txtNeName = (TextView)view.findViewById(R.id.txt_ne_name);
			holder.txtInfo = (TextView)view.findViewById(R.id.txt_info);
			holder.txtBscName = (TextView)view.findViewById(R.id.txt_bsc_name);
			holder.txtNeId = (TextView)view.findViewById(R.id.txt_bcf);
			holder.txtDate = (TextView)view.findViewById(R.id.txt_datetime);


			view.setTag(holder);
		}else {
			holder = (ViewHolder)view.getTag();
		}
		
		holder.txtNeName.setText(list.get(position).getNeName());
		holder.txtInfo.setText(list.get(position).getInfo());
		holder.txtBscName.setText(list.get(position).getBscName());
		holder.txtNeId.setText(list.get(position).getNeId());
		holder.txtDate.setText(list.get(position).getDateAlarm()+"/"+list.get(position).getTimeAlarm());

		
		return view;
		
	}
	
	static class ViewHolder{
		TextView txtNeName, txtBscName, txtNeId, txtInfo, txtDate, txtTime;
	}
	

}
